// If we're not in the context of a Web Worker, then don't do anything
if ("function" === typeof importScripts) {
  importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js');

  if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`);
    workbox.precaching.precacheAndRoute([]);

    const showNotification = () => {
      self.registration.showNotification('Background sync success!', {
        body: '🎉`🎉`🎉`'
      });
    };

    const bgSyncPlugin = new workbox.backgroundSync.Plugin(
      'dashboard-queue',
      {
        maxRetentionTime: 24 * 60, // Retry for max of 24 Hours (specified in minutes)
        callbacks: {
          queueDidReplay: showNotification
        }
      }
    );

    const networkWithBackgroundSync = new workbox.strategies.NetworkOnly({
      plugins: [bgSyncPlugin],
    });

    workbox.routing.registerRoute(
      /\/api\/add/,
      networkWithBackgroundSync,
      'POST'
    );
  } else {
    console.log(`Boo! Workbox didn't load 😬`);
  }
}

# How to use?

Run `npm install` to install packages

Run `npm start` to build & start project

Visit URL http://localhost:8081 to check demo. Make sure to
stop all old service workers and cleared all cache
before.
